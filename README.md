# Gestion des ressources de conteneur
------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

### Introduction à la surveillance de la santé des conteneurs avec des sondes

Bonjour et bienvenue dans cette leçon où nous allons parler de la surveillance de la santé des conteneurs avec des sondes. Voici un aperçu rapide des sujets que nous allons aborder :

1. Qu'est-ce que la santé des conteneurs ?
2. Les sondes Kubernetes :
   - Les sondes de vivacité (liveness probes)
   - Les sondes de démarrage (startup probes)
   - Les sondes de disponibilité (readiness probes)
3. Une démonstration pratique pour voir comment ces concepts se présentent dans un cluster Kubernetes.

### Qu'est-ce que la santé des conteneurs ?

Kubernetes offre plusieurs fonctionnalités permettant de construire des applications robustes, notamment la possibilité de redémarrer automatiquement les conteneurs défaillants. Pour tirer parti de ces fonctionnalités, Kubernetes doit être capable de déterminer avec précision l'état de nos applications. Cela implique de vérifier si nos conteneurs fonctionnent correctement ou s'ils sont défaillants.

- **Conteneurs sains** : Fonctionnent comme prévu.
- **Conteneurs défaillants** : Présentent des problèmes qui nécessitent une attention.

### Les sondes de vivacité (liveness probes)

Les sondes de vivacité permettent de déterminer automatiquement si une application de conteneur est en bon état. Par défaut, Kubernetes considère qu'un conteneur est défaillant si le processus du conteneur s'arrête. Les sondes de vivacité permettent de personnaliser ce mécanisme de détection pour détecter des problèmes plus subtils, même si le processus du conteneur est toujours en cours d'exécution.

### Les sondes de démarrage (startup probes)

Les sondes de démarrage sont similaires aux sondes de vivacité, mais elles ne s'exécutent qu'au démarrage du conteneur et s'arrêtent une fois qu'elles réussissent. Elles sont utilisées pour détecter quand une application a démarré avec succès, particulièrement utile pour les applications héritées qui ont des temps de démarrage très longs.

### Les sondes de disponibilité (readiness probes)

Les sondes de disponibilité déterminent quand un conteneur est prêt à accepter des requêtes. Elles fonctionnent pendant le processus de démarrage, mais leur objectif est différent. Elles empêchent le trafic utilisateur d'être dirigé vers un conteneur jusqu'à ce qu'il soit complètement prêt, assurant ainsi une meilleure expérience utilisateur.

### Démonstration pratique

Passons maintenant à une démonstration pratique pour explorer ces sondes dans un cluster Kubernetes.

1. **Création d'un pod avec une sonde de vivacité (liveness probe)** :

   ```yaml
   apiVersion: v1
   kind: Pod
   metadata:
     name: liveness-pod
   spec:
     containers:
     - name: busybox
       image: busybox
       args: ["sh", "-c", "sleep 3600"]
       livenessProbe:
         exec:
           command: ["echo", "Hello, World!"]
         initialDelaySeconds: 5
         periodSeconds: 5
   ```

   Cette configuration vérifie la vivacité du conteneur en exécutant une commande `echo`.

2. **Création d'un pod avec une sonde HTTP de vivacité** :

   ```yaml
   apiVersion: v1
   kind: Pod
   metadata:
     name: liveness-pod-http
   spec:
     containers:
     - name: nginx
       image: nginx
       livenessProbe:
         httpGet:
           path: /
           port: 80
         initialDelaySeconds: 5
         periodSeconds: 5
   ```

   Cette configuration utilise une requête HTTP GET pour vérifier la vivacité du conteneur.

3. **Création d'un pod avec une sonde de démarrage (startup probe)** :

   ```yaml
   apiVersion: v1
   kind: Pod
   metadata:
     name: startup-pod
   spec:
     containers:
     - name: nginx
       image: nginx
       startupProbe:
         httpGet:
           path: /
           port: 80
         initialDelaySeconds: 5
         periodSeconds: 5
         failureThreshold: 30
   ```

   Cette configuration utilise une sonde de démarrage pour vérifier que l'application a démarré avec succès.

4. **Création d'un pod avec une sonde de disponibilité (readiness probe)** :

   ```yaml
   apiVersion: v1
   kind: Pod
   metadata:
     name: readiness-pod
   spec:
     containers:
     - name: nginx
       image: nginx
       readinessProbe:
         httpGet:
           path: /
           port: 80
         initialDelaySeconds: 5
         periodSeconds: 5
   ```

   Cette configuration utilise une sonde de disponibilité pour s'assurer que le conteneur est prêt à accepter des requêtes.

### Conclusion

Pour récapituler, nous avons discuté de la santé des conteneurs, des sondes de vivacité, des sondes de démarrage et des sondes de disponibilité, et nous avons réalisé une démonstration pratique de ces concepts dans un cluster Kubernetes. C'est tout pour cette leçon. À la prochaine !


# Reférences



https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle/


https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle/#container-probes